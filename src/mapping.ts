import { Address, BigInt, ethereum, log } from "@graphprotocol/graph-ts"
import {
  MansionManager,
  Transfer as MansionTransferevent
} from "../generated/MansionManager/MansionManager"

import {
  DistrictManager,
  Transfer as DistrictTransferevent
} from "../generated/DistrictManager/DistrictManager"

import {
  DistrictManagerV2,
  Transfer as DistrictTransfereventV2
} from "../generated/DistrictManagerV2/DistrictManagerV2"

import {
  Playmates,
  Transfer as PlaymatesTransferevent
} from "../generated/Playmates/Playmates"

import {
  WavaxJLP,
  Swap as WavaxLPSwapevent
} from "../generated/WavaxJLP/WavaxJLP"

import { Bag, Mansion, District, Playmate, WavaxLP } from "../generated/schema"

export function handleMansionTransfer(event: MansionTransferevent): void {
  let bag = _loadBag(event.params.to.toHex())
  bag.save()

  let mansionId = event.params.tokenId
  let mansion = Mansion.load(mansionId.toHex())
  let timestamp = event.block.timestamp

  if (!mansion) {
    // Entity IDs need to be strings
    mansion = new Mansion(mansionId.toHex())
    mansion.tokenId = mansionId
    mansion.created = timestamp
    let contract = MansionManager.bind(event.address)
    let txHash = event.transaction.hash.toHexString()
    let mansionName = contract.try_getNameOf(mansionId)

    if (mansionName.reverted) {
      log.warning("mansionName try_getNameOf reverted at transaction #{}", [txHash])
    }
    else {
      mansion.name = mansionName.value
    }

    let mansionClaim = contract.try_getClaimOf(mansionId)
    if (mansionClaim.reverted) {
      log.warning("mansionClaim try_getClaimOf reverted at transaction #{}", [txHash])
    }
    else {
      mansion.claim = mansionClaim.value
    }

    let mansionMint = contract.try_getMintOf(mansionId)
    if (mansionMint.reverted) {
      log.warning("mansionMint try_getMintOf reverted at transaction #{}", [txHash])
    }
    else {
      mansion.mint = mansionMint.value
    }
  }

  mansion.bag = bag.id;
  mansion.oldOwner = event.params.from
  mansion.updated = timestamp
  mansion.save()
}

export function handleDistrictTransfer(event: DistrictTransferevent): void {
  
  let bag = _loadBag(event.params.to.toHex())
  bag.save()

  let districtId = event.params.tokenId
  let timestamp = event.block.timestamp
  let district = District.load(districtId.toHex())

  if (!district) {
    district = new District(districtId.toHex())
    district.tokenId = districtId
    district.created = timestamp
    let contract = DistrictManager.bind(event.address)
    let districtName = contract.try_getNameOf(districtId)
    let txHash = event.transaction.hash.toHexString()

    if (districtName.reverted) {
      log.warning("districtName try_getNameOf reverted at transaction #{}", [txHash])
    }
    else {
      district.name = districtName.value
    }
  }

  district.bag = bag.id;
  district.updated = timestamp
  district.save()
}

export function handleDistrictTransferV2(event: DistrictTransfereventV2): void {
  
  let bag = _loadBag(event.params.to.toHex())
  bag.save()

  let districtId = event.params.tokenId
  let timestamp = event.block.timestamp
  let district = District.load(districtId.toHex())

  if (!district) {
    district = new District(districtId.toHex())
    district.tokenId = districtId
    district.created = timestamp
    let contract = DistrictManagerV2.bind(event.address)
    let districtName = contract.try_getNameOf(districtId)
    let txHash = event.transaction.hash.toHexString()

    if (districtName.reverted) {
      log.warning("districtName try_getNameOf reverted at transaction #{}", [txHash])
    }
    else {
      district.name = districtName.value
    }
  }

  district.bag = bag.id;
  district.updated = timestamp
  district.save()
}

// TODO, unify district's contract versions logic
// export function _handleDistrictTransferEvent(contract: ethereum.SmartContract,  event: ethereum.Event) {
//   let bag = _loadBag(event.params.to.toHex())
//   bag.save()

//   let districtId = event.params.tokenId
//   let timestamp = event.block.timestamp
//   let district = District.load(districtId.toHex())

//   if (!district) {
//     district = new District(districtId.toHex())
//     district.tokenId = districtId
//     district.created = timestamp
//     let districtContract = contract.bind(event.address)
//     let districtName = districtContract.try_getNameOf(districtId)
//     let txHash = event.transaction.hash.toHexString()

//     if (districtName.reverted) {
//       log.warning("districtName try_getNameOf reverted at transaction #{}", [txHash])
//     }
//     else {
//       district.name = districtName.value
//     }
//   }

//   district.bag = bag.id;
//   district.updated = timestamp
//   district.save()
// }

export function handlePlaymatesTransfer(event: PlaymatesTransferevent): void {

  // address payable public marketingWallet = payable(0x8c400d07c258e07681587d3dbdc3df1ce3306dd7);
  // NOT WORKING (TEST) -> address payable private rewardsPool = payable(0xa3b4c11e2625b3a39c838da649a28b00f3c49cce);

  // PLAYMATES CONTRACT 0x490bf3abcab1fb5c88533d850f2a8d6d38298465
  // PLAYMATES Reward Pool - ERC20 TOKEN TX 0x58ce8d0bc76bf7a4ea726e1477bdf9cfb9a7802e

  // WAVAX 0xb31f66aa3c1e785363f0875a1b74e27b85fd66c7
  // JOE LP Liquidity pool??? (JLP/PLAYMATES/WAVAX) 0xfd17c7a2e1eee2ee2a308e43bdfbac4df135c5cd
  // Mansion helper 0x1ddf6b658003e7846b1a8febcc7aefe99c237098

  // Vault MIM/PLAYMATES Vault 0x580a86d85a0fcdde272dd15ec9bc4b9f8202a6ae
  // treasuryWallet address PLAYMATES/MIM = payable(0xe63fb42e89cd67d93bcb6dc5e9388b347e3174be);

  // Vault for MIM Contract
  // // MARKETING
  // 0x8c400d07c258e07681587d3dbdc3df1ce3306dd7 
  // // PAYMENT_TOKEN
  // 0x130966628846bfd36ff31a822705796e8cb8c18d 
  // // PLAYMATES
  // 0x490bf3abcab1fb5c88533d850f2a8d6d38298465 
  // // POOL
  // 0xae822c161081a822394cc7c76916b09ddd8f77cd 
  // // TREASURY
  // 0xe63fb42e89cd67d93bcb6dc5e9388b347e3174be 
  // // TRUST_V1
  // 0x8e297bd2b6a71689333f8d0f8474539ecf80cadf 

  let bag = _loadBag(event.params.to.toHex())
  bag.save()
  let oldOwnerBag = _loadBag(event.params.from.toHex())
  oldOwnerBag.save()
  let amount = event.params.value
  
  let timestamp = event.block.timestamp
  let playmateid = _createIndexId(event)
  let playmate = _loadPlaymate(playmateid, timestamp)

  playmate.oldOwner = event.params.from
  playmate.bag = bag.id
  playmate.amount = amount
  playmate.updated = timestamp
  // @TODO
  // Is swap?
  
  // Is autoliqufy?
  playmate.save()

  // Update bag balances
  _setPlaymatesReceivedBalance(bag, amount)
  bag.save()

  _setPlaymatesSentBalance(oldOwnerBag, amount)
  oldOwnerBag.save()
}

export function handleWavaxJLPSwap(event: WavaxLPSwapevent): void {
  // Swap(msg.sender, amount0In, amount1In, amount0Out, amount1Out, to);
  // msg.sender might not be the bag who sent the transaction
  // we take it better from the transaction itself.
  let swapOwnerBag = event.transaction.from
  // swapOwnerBag sending amount1In of AVAX for swap.
  let amount1In = event.params.amount1In
  // swapOwnerBag receiving amount1Out of AVAX from swap.
  let amount1Out = event.params.amount1Out
  // Initialization (it will be overriten)
  let avaxAmount = BigInt.fromU32(0)

  // We only check for incoming/outcoming wavax.
    if(!amount1In.isZero() || !amount1Out.isZero()) {
      let bag = _loadBag(swapOwnerBag.toHex())

      // AVAX for PM
      if (!amount1In.isZero()) {
        bag.wavaxlpbalance = bag.wavaxlpbalance - amount1In
        bag.wavaxlpsent = bag.wavaxlpsent + amount1In
        avaxAmount = amount1In
      }
      // PM for AVAX
      else if (!amount1Out.isZero()) {
        bag.wavaxlpreceived = bag.wavaxlpreceived + amount1Out
        bag.wavaxlpbalance = bag.wavaxlpbalance + amount1Out
        avaxAmount = amount1Out
      }

      bag.save()

      let timestamp = event.block.timestamp
      let wavaxlpid = _createIndexId(event)
      let wavaxlp = _loadWavaxLP(wavaxlpid, timestamp)
      wavaxlp.bag = bag.id
      wavaxlp.amount = avaxAmount
      wavaxlp.updated = timestamp

      wavaxlp.save()
  }
}

//--- Bags ---//
// Loads or creates a Bag entity.
export function _loadBag(addres: string): Bag {
  let bag = Bag.load(addres)
  if (!bag) {
    bag = new Bag(addres)
  }

  return bag
}

//--- Playmates ---//
export function _loadPlaymate(playmateid: string, blocktimestamp: BigInt): Playmate {
  let playmate = Playmate.load(playmateid)
  if (!playmate) {
    playmate = new Playmate(playmateid)
    playmate.created = blocktimestamp
  }
  return playmate
}

export function _setPlaymatesReceivedBalance(bag: Bag, amount: BigInt): void {
  bag.pmreceived = bag.pmreceived + amount
  bag.balance = bag.balance + amount
}

export function _setPlaymatesSentBalance(bag: Bag, amount: BigInt): void {
  bag.pmsent = bag.pmsent + amount
  bag.balance =  bag.balance - amount
}

//--- Wavax/Playmates Liquidity Pool ---//
export function _loadWavaxLP(wavaxlpid: string, blocktimestamp: BigInt): WavaxLP {
  let wavaxlp = WavaxLP.load(wavaxlpid)
  if (!wavaxlp) {
    wavaxlp = new WavaxLP(wavaxlpid)
    wavaxlp.created = blocktimestamp
  }
  return wavaxlp
}

// Gets a unique id for the entity.
export function _createIndexId(event: ethereum.Event): string {
  // Same transaction could fire more than one transfer event
  let indexId = event.transaction.hash.toHex().toLowerCase() + "-" + event.logIndex.toString()
  return indexId
}


// export function handleApproval(event: Approval): void {
//   // Entities can be loaded from the store using a string ID; this ID
//   // needs to be unique across all entities of the same type
//   let entity = ExampleEntity.load(event.transaction.from.toHex())

//   // Entities only exist after they have been saved to the store;
//   // `null` checks allow to create entities on demand
//   if (!entity) {
//     entity = new ExampleEntity(event.transaction.from.toHex())

//     // Entity fields can be set using simple assignments
//     entity.count = BigInt.fromI32(0)
//   }

//   // BigInt and BigDecimal math are supported
//   entity.count = entity.count + BigInt.fromI32(1)

//   // Entity fields can be set based on event parameters
//   entity.owner = event.params.owner
//   entity.approved = event.params.approved

//   // Entities can be written to the store with `.save()`
//   entity.save()

//   // Note: If a handler doesn't require existing field values, it is faster
//   // _not_ to load the entity from the store. Instead, create it fresh with
//   // `new Entity(...)`, set the fields that should be updated and save the
//   // entity back to the store. Fields that were not set or unset remain
//   // unchanged, allowing for partial updates to be applied.

//   // It is also possible to access smart contracts from mappings. For
//   // example, the contract that has emitted the event can be connected to
//   // with:
//   //
//   // let contract = Contract.bind(event.address)
//   //
//   // The following functions can then be called on this contract to access
//   // state variables and other data:
//   //
//   // - contract.balanceOf(...)
//   // - contract.balanceOf(...)
//   // - contract.cityMultiplier(...)
//   // - contract.claim(...)
//   // - contract.claimTime(...)
//   // - contract.deadAddress(...)
//   // - contract.defaultUri(...)
//   // - contract.districtMultiplier(...)
//   // - contract.foundersMultiplier(...)
//   // - contract.foundersNFT(...)
//   // - contract.getAddressRewards(...)
//   // - contract.getApproved(...)
//   // - contract.getCityMultiplier(...)
//   // - contract.getClaimOf(...)
//   // - contract.getDistrictMultiplier(...)
//   // - contract.getFoundersMultiplier(...)
//   // - contract.getMansions(...)
//   // - contract.getMansionsOf(...)
//   // - contract.getMintOf(...)
//   // - contract.getNameOf(...)
//   // - contract.getRewardOf(...)
//   // - contract.getUpgradeNodeCount(...)
//   // - contract.getUserMultiplier(...)
//   // - contract.helperContract(...)
//   // - contract.isApprovedForAll(...)
//   // - contract.isBlacklisted(...)
//   // - contract.name(...)
//   // - contract.owner(...)
//   // - contract.ownerOf(...)
//   // - contract.precision(...)
//   // - contract.price(...)
//   // - contract.reward(...)
//   // - contract.supportsInterface(...)
//   // - contract.symbol(...)
//   // - contract.tokenURI(...)
//   // - contract.totalNodesCreated(...)
//   // - contract.transferIsEnabled(...)
//   // - contract.upgradeManager(...)
//   // - contract.vbuy1(...)
//   // - contract.vbuy2(...)
//   // - contract.vbuy3(...)
//   // - contract.vbuy4(...)
//   // - contract.vbuy5(...)
//   // - contract.vbuy6(...)
// }

// export function handleApprovalForAll(event: ApprovalForAll): void {}

// export function handleContractOwnershipTransferred(
//   event: ContractOwnershipTransferred
// ): void {}

// export function handleOwnershipTransferred(event: OwnershipTransferred): void {}

// export function handleTransfer(event: Transfer): void {}